import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:scoped_model/scoped_model.dart';
import 'scoped-model/main.dart';

//import 'package:flutter/rendering.dart';

import './pages/products_admin.dart';
import './pages/products.dart';
import './pages/product.dart';
import './pages/auth.dart';
import './models/product.dart';

//debugPaintSizeEnabled = true;
//debugPaintBaselinesEnabled = true;
//debugPaintPointerEnabled = true;
void main() {
  // debugPaintSizeEnabled = true;
//debugPaintBaselinesEnabled = true;
//debugPaintPointerEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final MainModel model = MainModel();
    return ScopedModel<MainModel>(
      model: model,
      child: MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        buttonColor:  Theme.of(context).accentColor,
      ),
      // home: AuthPage(),
      routes: {
        '/': (BuildContext context) => AuthPage(),
        '/admin': (BuildContext context) => ProductsAdminpage(model),
        '/product': (BuildContext context) => ProductsPage(model),
      },
      onGenerateRoute: (RouteSettings setings) {
        final List<String> pathElements = setings.name.split('/');
        if (pathElements[0] != '') {
          return null;
        }
        if (pathElements[1] == 'product') {
          final String productId = pathElements[2];
          final Product product = model.allproducts.firstWhere((Product value){
              return value.id == productId;
          });
          return MaterialPageRoute<bool>(
              builder: (BuildContext context) => ProductPage(product));
        }

        return null;
      },
      onUnknownRoute: (RouteSettings seting) {
        return MaterialPageRoute(
            builder: (BuildContext context) => ProductsPage(model));
      },
    ) ,);
  }
}
