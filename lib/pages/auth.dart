import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../scoped-model/main.dart';

enum AuthMode { Siginup, Login }

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> {
  final Map<String, dynamic> _formData = {
    "email": null,
    "password": null,
    "acceptTerms": false
  };

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordTextController = TextEditingController();
  AuthMode _authMode = AuthMode.Login;

  DecorationImage _buildBackgroundImage() {
    return DecorationImage(
        fit: BoxFit.cover,
        colorFilter:
            ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
        image: AssetImage('assets/background.jpg'));
  }

  Widget _buildEmailTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "E-mail",
          filled: true,
          fillColor: Colors.white.withOpacity(0.7)),
      keyboardType: TextInputType.emailAddress,
      validator: (String value) {
        if (value.isEmpty ||
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return "Please enter email";
        }
      },
      onSaved: (String value) {
        _formData["email"] = value;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
        decoration: InputDecoration(
            labelText: "Password",
            filled: true,
            fillColor: Colors.white.withOpacity(0.7)),
        obscureText: true,
        controller: _passwordTextController,
        validator: (String value) {
          if (value.isEmpty || value.length < 6) {
            return "Password invalid";
          }
        },
        onSaved: (String value) {
          _formData["password"] = value;
        });
  }

  Widget _buildPasswordConfirmTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "Confirm Password",
          filled: true,
          fillColor: Colors.white.withOpacity(0.7)),
      obscureText: true,
      validator: (String value) {
        if (_passwordTextController.text != value) {
          return "Password not match";
        }
      },
      onSaved: (String value) {
        _formData["email"] = value;
      },
    );
  }

  Widget _buildAcceptSwitch() {
    return SwitchListTile(
      value: _formData["acceptTerms"],
      onChanged: (bool value) {
        setState(() {
          _formData["acceptTerms"] = value;
        });
      },
      title: Text(
        "Accept terms",
      ),
    );
  }

  void _submitLogin(Function login) {
    if (!_formKey.currentState.validate() || !_formData["acceptTerms"]) {
      return;
    }
    _formKey.currentState.save();
    login(_formData["email"], _formData["password"]);

    Navigator.pushReplacementNamed(context, '/products');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Container(
          decoration: BoxDecoration(image: _buildBackgroundImage()),
          padding: EdgeInsets.all(10.0),
          child: Center(
            child: SingleChildScrollView(
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Form(
                      key: _formKey,
                      child: Column(children: <Widget>[
                        _buildEmailTextField(),
                        SizedBox(
                          height: 10.9,
                        ),
                        _buildPasswordTextField(),
                        SizedBox(
                          height: 10.0,
                        ),
                        _authMode == AuthMode.Siginup
                            ? _buildPasswordConfirmTextField()
                            : Container(),
                        _buildAcceptSwitch(),
                        SizedBox(
                          height: 10.0,
                        ),
                        FlatButton(
                          child: Text(
                              "Switch to ${_authMode == AuthMode.Login ? "Signup" : "Login"}"),
                          onPressed: () {
                            setState(() {
                              _authMode = _authMode == AuthMode.Login
                                  ? AuthMode.Siginup
                                  : AuthMode.Login;
                            });
                          },
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        ScopedModelDescendant<MainModel>(
                          builder: (BuildContext context, Widget child,
                              MainModel model) {
                            return RaisedButton(
                              child: Text("Login"),
                              onPressed: () => _submitLogin(model.login),
                            );
                          },
                        ),
                      ]),
                    ))),
          )),
    );
  }
}
