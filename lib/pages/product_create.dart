import 'package:flutter/material.dart';

class ProductCreatePage extends StatefulWidget {
  final Function addProduct;

  ProductCreatePage(this.addProduct);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductCreatePageState();
  }
}

class _ProductCreatePageState extends State<ProductCreatePage> {
  final  Map<String,dynamic> _formData = {
    "title":null,
    "description":null,
    "price":null,
    "image": "assets/law.jpg"

  };
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildTitleTextField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: "Product title",
      ),
      validator: (String value) {
        if (value.isEmpty || value.length < 5) {
          return "Title is required it should be 5+ chars lenght";
        }
      },
      onSaved: (String value) {

          _formData["title"] = value;

      },
    );
  }

  Widget _buildDescriptionTextField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: "Product description",
      ),
      maxLines: 4,
      validator: (String value) {
        if (value.isEmpty || value.length < 10) {
          return "Description is required an should be 10 + characters long ";
        }
      },
      onSaved: (String value) {

          _formData["description"] = value;

      },
    );
  }

  Widget _buidProductPriceTextField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: "Product price",
      ),
      keyboardType: TextInputType.number,
      validator: (String value) {
        if (value.isEmpty || !RegExp(r'^(?:[1-9]\d*|0)?(?:\.\d+)?$').hasMatch(value)) {
          return "Price is required an shuld be num";
        }
      },
      onSaved: (String value) {


          _formData["price"] = double.parse(value);

      },
    );
  }

  void _submitForm() {
    if (!_formKey.currentState.validate()) {

     return;
    }
    _formKey.currentState.save();
    widget.addProduct(_formData);
    _formKey.currentState.reset();
    Navigator.pushReplacementNamed(context, '/products');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500 : deviceWidth * 0.95;
    final double targetPadding = deviceWidth - targetWidth;

    return GestureDetector(
      onTap: (){
        FocusScope.of(context).requestFocus(FocusNode());
      },
        child: Container(
      margin: EdgeInsets.all(10.0),
      child: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: targetPadding / 2),
          children: <Widget>[
            _buildTitleTextField(),
            _buildDescriptionTextField(),
            _buidProductPriceTextField(),
            SizedBox(
              height: 10.0,
            ),
            RaisedButton(
              child: Text("create"),
              textColor: Colors.white,
              onPressed: _submitForm,
            )
            /*  GestureDetector(
              onTap: _submitForm,
              child: Container(
                padding: EdgeInsets.all(5.0),
                color: Colors.indigo[300],
                child: Text("My Buuton"),
              ),
            )*/
          ],
        ),
      ),
    ));
  }
}
