import 'package:flutter/material.dart';
import 'package:test123/models/product.dart';
import '../widgets/helpers/ensure-visible.dart';
import 'package:scoped_model/scoped_model.dart';
import '../scoped-model/main.dart';

class ProductEditPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductEditPageState();
  }
}

class _ProductEditPageState extends State<ProductEditPage> {
  final Map<String, dynamic> _formData = {
    "title": null,
    "description": null,
    "price": null,
    "image": "assets/law.jpg"
  };
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _titleFocusedNode = FocusNode();
  final _descriptionFocuseedNode = FocusNode();
  final _priceFocusedNode = FocusNode();

  Widget _buildTitleTextField(Product product) {
    return EnsureVisibleWhenFocused(
      focusNode: _titleFocusedNode,
      child: TextFormField(
        focusNode: _titleFocusedNode,
        decoration: InputDecoration(
          labelText: "Product title",
        ),
        initialValue: product == null ? '' : product.title,
        validator: (String value) {
          if (value.isEmpty || value.length < 5) {
            return "Title is required it should be 5+ chars lenght";
          }
        },
        onSaved: (String value) {
          _formData["title"] = value;
        },
      ),
    );
  }

  Widget _buildDescriptionTextField(Product product) {
    return EnsureVisibleWhenFocused(
        focusNode: _descriptionFocuseedNode,
        child: TextFormField(
          focusNode: _descriptionFocuseedNode,
          decoration: InputDecoration(
            labelText: "Product description",
          ),
          maxLines: 4,
          initialValue: product == null ? '' : product.description.toString(),
          validator: (String value) {
            if (value.isEmpty || value.length < 10) {
              return "Description is required an should be 10 + characters long ";
            }
          },
          onSaved: (String value) {
            _formData["description"] = value;
          },
        ));
  }

  Widget _buidProductPriceTextField(Product product) {
    return EnsureVisibleWhenFocused(
        focusNode: _priceFocusedNode,
        child: TextFormField(
          focusNode: _priceFocusedNode,
          decoration: InputDecoration(
            labelText: "Product price",
          ),
          keyboardType: TextInputType.number,
          initialValue: product == null ? '' : product.price.toString(),
          validator: (String value) {
            if (value.isEmpty ||
                !RegExp(r'^(?:[1-9]\d*|0)?(?:\.\d+)?$').hasMatch(value)) {
              return "Price is required an shuld be num";
            }
          },
          onSaved: (String value) {
            _formData["price"] = double.parse(value);
          },
        ));
  }

  void _submitForm(
      Function addProduct, Function upddateProduct, Function setSelectedProduct,
      [int selectedProductIndex]) {
    if (!_formKey.currentState.validate()) {
      return;
    }

    _formKey.currentState.save();
    if (selectedProductIndex == -1) {
      addProduct(_formData["title"], _formData["description"],
              _formData["image"], _formData["price"])
          .then((bool sueccess) {
        if (sueccess) {
          Navigator.pushReplacementNamed(context, '/products')
              .then((_) => setSelectedProduct(null));
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Something went wrong"),
                  content: Text("Please try again"),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text("OK"),
                    )
                  ],
                );
              });
        }
      });
    } else {
      upddateProduct(_formData["title"], _formData["description"],
          _formData["image"], _formData["price"])
        ..then((_) => Navigator.pushReplacementNamed(context, '/products')
            .then((_) => setSelectedProduct(null)));
    }
  }

  Widget _buildPageContent(BuildContext context, Product product) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500 : deviceWidth * 0.95;
    final double targetPadding = deviceWidth - targetWidth;
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          margin: EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: targetPadding / 2),
              children: <Widget>[
                _buildTitleTextField(product),
                _buildDescriptionTextField(product),
                _buidProductPriceTextField(product),
                SizedBox(
                  height: 10.0,
                ),
                _buildSubmitButton()
                /*  GestureDetector(
              onTap: _submitForm,
              child: Container(
                padding: EdgeInsets.all(5.0),
                color: Colors.indigo[300],
                child: Text("My Buuton"),
              ),
            )*/
              ],
            ),
          ),
        ));
  }

  Widget _buildSubmitButton() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : RaisedButton(
                child: Text("create"),
                textColor: Colors.white,
                onPressed: () => _submitForm(
                    model.addProduct,
                    model.updateProduct,
                    model.selectProduct,
                    model.selectedProductIndex),
              );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      final Widget pageContent =
          _buildPageContent(context, model.selectedProduct);
      return model.selectedProductIndex == -1
          ? pageContent
          : Scaffold(
              appBar: AppBar(
                title: Text("Edit product"),
              ),
              body: pageContent,
            );
    });
  }
}
