import 'package:flutter/material.dart';


class PriceTeg extends StatelessWidget{

  final String price;

  PriceTeg(this.price);

  @override
   Widget build(BuildContext context) {
    // TODO: implement build
    return  Container(
      padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.5),
      decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(5.0)),
      child: Text(
        "\$$price",
        style: TextStyle(color: Colors.white),

      ),
    );
  }



}