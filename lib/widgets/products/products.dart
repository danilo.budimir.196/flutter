import 'package:flutter/material.dart';
import 'package:test123/models/product.dart';
import './products_card.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:test123/scoped-model/main.dart';
class Products extends StatelessWidget {


  Widget _buildProductslist(List<Product> products) {
    Widget productCard = Center(
      child: Text("no products"),
    );

    if (products.length > 0) {
      productCard = ListView.builder(
        itemBuilder: (BuildContext context, int index) =>
            ProductsCard(products[index], index),
        itemCount: products.length,
      );
    } else {
      productCard = Center(
        child: Text(""),
      );
    }

    return productCard;
  }

  @override
  Widget build(BuildContext context) {
    print("[Products Widget ] build");
    return ScopedModelDescendant<MainModel>(builder: (BuildContext context,Widget child,MainModel model){

        return _buildProductslist(model.dysplayedProducts);
    },);
  }
}
