import 'package:flutter/material.dart';
import 'package:test123/models/product.dart';
import './price_tag.dart';
import '../ui_elements/title_default.dart';
import './address_tag.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../scoped-model/main.dart';

class ProductsCard extends StatelessWidget {
  final Product product;
  final int productIndex;

  ProductsCard(this.product, this.productIndex);

  Widget _buildTitlePriceRow() {
    return Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TitleDefault(product.title),
            SizedBox(
              width: 8.0,
            ),
            PriceTeg(product.price.toString())
          ],
        ));
  }

  Widget _buildActionsButtons(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.info),
                color: Theme.of(context).primaryColor,
                onPressed: () => Navigator.pushNamed<bool>(
                    context, '/product/' + model.allproducts[productIndex].id),
              ),
              IconButton(
                  icon: Icon(model.allproducts[productIndex].isFavorite
                      ? Icons.favorite
                      : Icons.favorite_border),
                  color: Colors.red,
                  onPressed: () {
                    model.selectProduct(model.allproducts[productIndex].id);
                    model.toggleProductFaforiteStatus();
                  }),
            ]);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage(product.image),
            height: 300.0,
            fit: BoxFit.cover,
            placeholder: AssetImage("assets/background.jpg"),
          ),
          _buildTitlePriceRow(),
          AddressTag("Jovana Boskovica 121"),
          Text(product.userEmail),
          _buildActionsButtons(context)
        ],
      ),
    );
  }
}
